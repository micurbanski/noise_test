function [ wynik ] = BATCHWrite(catalog, n)
%BATCHWRITE prepare batch files in catalog for core n

a{1}='#!/bin/sh -f';
a{2}=['taskset -c ',num2str(n,'%i'),' cat perm_p1 perm_p2 perm_p3 > permeab.f90'];
a{3}=['taskset -c ',num2str(n,'%i'),' elmerf90 permeab.f90 -o perme'];
a{4}=['taskset -c ',num2str(n,'%i'),' touch phase1_done.txt'];

nazwa=[catalog '/phase1_go'];

FID = fopen(nazwa, 'w');
fprintf(FID, '%s\n', a{:});
fclose(FID);

[output, text]=system(["chmod 755 " nazwa]);    

a{1}='#!/bin/sh -f';
% a{2}=['taskset -c ',num2str(n,'%i'),' ElmerGrid 14 2 solenoid_spindle_harmonic.msh -autoclean'];
a{2}=['taskset -c ',num2str(n,'%i'),' ElmerSolver'];
a{3}=['taskset -c ',num2str(n,'%i'),' touch phase2_done.txt'];

nazwa=[catalog '/phase2_go'];

FID = fopen(nazwa, 'w');
fprintf(FID, '%s\n', a{:});
fclose(FID);

[output, text]=system(["chmod 755 " nazwa]);    

wynik=1;

end

