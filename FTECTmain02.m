clear all;
diary off;

setenv("OMP_NUM_THREADS","1");

page_screen_output(0);
page_output_immediately(1);

% -------------------------------------
% 
% Control data set:

y_min=-0.02;
y_max=0.02;
y_step=0.0002;        % linear movement table

fi_min=0;
fi_step=1;         % rotation control table
fi_max=0;

NoCores = 7;        % number of working cores. Core "0" for Octave

MaxTimeCalc = 3000;  % maximal time for calculations (s)

Time_st=time();
Time_pt=time();

% -------------------------------------

fprintf('\n\n\n*******************************************************\n');
fprintf('*** Eddy Current Tomographic Forward Transformation *** \n');
fprintf('******************************************************* \n\n');

fprintf('Input data:\n\n');
fprintf('Linear steps: %1.4f : %1.4f : %1.4f \n',y_min, y_step, y_max);
fprintf('Rotation steps: %1.3f : %2.3f : %3.3f \n\n',fi_min, fi_step, fi_max);


fprintf('Number of working cores: %1.0f  Core 0 reserved for Octave \n\n',NoCores);

WorkDir = ['Working directory: ' pwd()];
disp(WorkDir);
WorkDir = pwd();

fprintf('\n---------------------------------- ');
fprintf('\n\nControl matrixes preparation... ');

InputMatrix=[];

for y=y_min:y_step:y_max,		% linear positions
    for fi=fi_min:fi_step:fi_max,       % rotation
        InputMatrix=[InputMatrix; y fi];
    end
end

OutputMatrix=zeros(size(InputMatrix));  % Fi_re Fi_im

ControlMatrix=zeros(size(InputMatrix,1),9); 
% (1) state: 0 - nothing, 1 - running, 2 - ready, (-1) - malfunction
% (2) no of phase: 0 - nothing, 1 - phase 1, 2 - phase 2, 3 - ready
% (3) correction required: 0 - no, 1 -yes
% (4) corrections: 0 - no correction, 1 - repeated, 2 - +1 deg, 3 - +2 deg, 4 - +5 deg
% (5) phase 1 start time
% (6) phase 2 end time
% (7) process number: in
% (8) process number: out
% (9) process number: pid

CoresMatrix=zeros(NoCores,3);

% (1) state: 0 - iddle, 1 - busy
% (2) no of work (from Input Matrix)
% (3) no of phase (1 or 2)

fprintf('ok.\n');
fprintf('Number of cycles: %5.0f \n',size(InputMatrix,1));

% --------------------------------------------

fprintf('\nPreparation of disk space environment... ');

for i=1:NoCores,
    DirName=['Work',num2str(i,'%03i')];
    [status, msg, msgid] = mkdir(WorkDir,DirName);
    D1=[WorkDir '/Work/*.*'];
    D2=[WorkDir '/' DirName];
    copyfile(D1,D2,'f');
    D1=[WorkDir '/Work/*'];
    copyfile(D1,D2,'f');
    wyn=BATCHWrite(D2,i);
    fprintf('\nCore: %i Directory: %s ',i,DirName);
end;

clear D1 D2 DirName status msg msgid y fi wyn;

fprintf('\n ok.\n');


% -----------------------------------------
% --- Main loop of calculation controll ---
% -----------------------------------------

WorkNotFinished=1;

while(WorkNotFinished)

% point 1 - find free core and assign new process

FreeCores = find(CoresMatrix(:,1)==0);
ProcesToStart = find(ControlMatrix(:,1)==0);

Nfc=size(FreeCores,1);
Npts=size(ProcesToStart,1);

if ((Nfc > 0) && (Npts > 0))
    
    if (Nfc<=Npts)
        Nop=Nfc;
    else
        Nop=Npts;
    end;

    
    for i=1:Nop,  % assign process

        y=InputMatrix(ProcesToStart(i),1);
        fi=InputMatrix(ProcesToStart(i),2); % input data
        
        cor_req=ControlMatrix(ProcesToStart(i),3);    % corrections
        correction=ControlMatrix(ProcesToStart(i),4);
        if ((cor_req==1) && (correction==2))
            fi=fi+1;
        end
        if ((cor_req==1) && (correction==3))
            fi=fi+2;
        end        
        if ((cor_req==1) && (correction==4))
            fi=fi+5;
        end 
                
        core=FreeCores(i);                      % given free core
            
        [PSt, in, out, pid]=StartProcess(y,fi,core); % Start the phase 1
        
        CoresMatrix(core,1)=1;                  % working
        CoresMatrix(core,2)=ProcesToStart(i);   % no of point
        CoresMatrix(core,3)=1;                  % phase 1
        
        ControlMatrix(ProcesToStart(i),1)=1;
        ControlMatrix(ProcesToStart(i),2)=1;
        ControlMatrix(ProcesToStart(i),5)=PSt;
        ControlMatrix(ProcesToStart(i),7)=in;
        ControlMatrix(ProcesToStart(i),8)=out;
        ControlMatrix(ProcesToStart(i),9)=pid;
        

    end
        
else
    % no free cores for process
end


% point 2 - check the state of processes in phase 1 
%    move to phase 2
%    finish process on phase 1 with malfunction flag: core is free and 
%    proces is repeated

ProcesToPhase2 = find(CoresMatrix(:,3)==1);

if (size(ProcesToPhase2,1) > 0)
    
    for i=1:size(ProcesToPhase2,1),  % assign process
        ProcesNum=CoresMatrix(ProcesToPhase2(i),2); % get no. of work
        ProcesCore=ProcesToPhase2(i);
        
        DirName=['Work',num2str(ProcesCore,'%03i')];
        D2=[WorkDir '/' DirName];
        
        Ph1ResF=[WorkDir '/' DirName '/permeab.f90'];
        Ph1FlagF=[WorkDir '/' DirName '/phase1_done.txt'];

        Ph1Res=exist(Ph1ResF);
        Ph1Flag=exist(Ph1FlagF);
        
        if (Ph1Flag)      % is process finished?
            
            fclose(ControlMatrix(ProcesNum,7));
            fclose(ControlMatrix(ProcesNum,8));
            waitpid(ControlMatrix(ProcesNum,9));    % zamknij
            [err,msg]=unlink(Ph1FlagF);
            
            if (Ph1Res)  % is process finished succesfully?
            
            [in, out, pid]=StartPhase2(ProcesCore); % Start the phase 2
        
            CoresMatrix(ProcesCore,3)=2;     % phase 2
      
            ControlMatrix(ProcesNum,2)=2;   % phase 2

            ControlMatrix(ProcesNum,7)=in;
            ControlMatrix(ProcesNum,8)=out;
            ControlMatrix(ProcesNum,9)=pid;
                                             % process moved to phase 2
            
            else % is process finished NOT succesfully?
 
                 % process will be repeated 
            CoresMatrix(ProcesCore,1)=0;     % reset
            CoresMatrix(ProcesCore,3)=0;
            ControlMatrix(ProcesNum,1)=0;    % reset
            ControlMatrix(ProcesNum,2)=0;    % reset
            ControlMatrix(ProcesNum,3)=1;    % repeated
            ControlMatrix(ProcesNum,4)=ControlMatrix(ProcesNum,4)+1;    
                                             % increase correction
             
            end
          
        else            % process is not finished    
            
            TimePh1=time()- ControlMatrix(ProcesNum,5);
            
            if (TimePh1>MaxTimeCalc)    % process is to long?  
                
                fprintf("\n *** Phase 1 to long. Process %i core %i will be repeated. \n", ProcesNum, ProcesCore);
         		
                kill_subs(ControlMatrix(ProcesNum,9));
                fclose(ControlMatrix(ProcesNum,7));
                fclose(ControlMatrix(ProcesNum,8));
                waitpid(ControlMatrix(ProcesNum,9));    % zamknij
                
                CoresMatrix(ProcesCore,1)=0;     % reset
                CoresMatrix(ProcesCore,3)=0;
                ControlMatrix(ProcesNum,1)=0;    % reset
                ControlMatrix(ProcesNum,2)=0;    % reset
                ControlMatrix(ProcesNum,3)=1;    % repeated
                ControlMatrix(ProcesNum,4)=ControlMatrix(ProcesNum,4)+1;
                                      % increase correction
                if (ControlMatrix(ProcesNum,4)>=5)
                    ControlMatrix(ProcesNum,1)=(-1);    % enough
                end
                [err,msg]=unlink(Ph1FlagF);
   
            end
                         
        end
        
    end
    
end
      
        
        
% point 3 - check the state of processes in phase 2
%    finish with Fi calculation and core is free
%    finish process on phase 2 with malfunction flag: core is free and
%    process is repeated

ProcesReady = find(CoresMatrix(:,3)==2);

if (size(ProcesReady,1) > 0)
    
    for i=1:size(ProcesReady,1),  % assign process

        ProcesNum=CoresMatrix(ProcesReady(i),2); % get no. of work
        ProcesCore=ProcesReady(i);
        
        DirName=['Work',num2str(ProcesCore,'%03i')];
        D2=[WorkDir '/' DirName];
        
        Ph2ResF=[WorkDir '/' DirName '/magfield.dat'];
        Ph2FlagF=[WorkDir '/' DirName '/phase2_done.txt'];

        Ph2Res=exist(Ph2ResF);
        Ph2Flag=exist(Ph2FlagF);
        
        if (Ph2Flag)      % is process finished?
            
            fclose(ControlMatrix(ProcesNum,7));
            fclose(ControlMatrix(ProcesNum,8));
            waitpid(ControlMatrix(ProcesNum,9));    % zamknij
            [err,msg]=unlink(Ph2FlagF);
            
            if (Ph2Res)  % is process finished succesfully?
            
            [Fi_re,Fi_im]=calculateFi(Ph2ResF); % Calculate Fi
            OutputMatrix(ProcesNum,1)=Fi_re;
            OutputMatrix(ProcesNum,2)=Fi_im;
        
            CoresMatrix(ProcesCore,1)=0;     % core is free
            CoresMatrix(ProcesCore,3)=0;
            ControlMatrix(ProcesNum,1)=2;    % process is done
            ControlMatrix(ProcesNum,2)=3;    % process is done
            ControlMatrix(ProcesNum,6)=time();    % time of end                                             
            
            else % is process finished NOT succesfully?
 
                 % process will be repeated 
            CoresMatrix(ProcesCore,1)=0;     % reset
            CoresMatrix(ProcesCore,3)=0;
            ControlMatrix(ProcesNum,1)=0;    % reset
            ControlMatrix(ProcesNum,2)=0;    % reset
            ControlMatrix(ProcesNum,3)=1;    % repeated
            ControlMatrix(ProcesNum,4)=ControlMatrix(ProcesNum,4)+1;
                                              % increase correction
            if (ControlMatrix(ProcesNum,4)>=5)
                ControlMatrix(ProcesNum,1)=(-1);    % enough
            end
                                                          
            end
          
        else            % process is not finished    
            
            TimePh2=time()- ControlMatrix(ProcesNum,5);
            
            if (TimePh2>(1.5.*MaxTimeCalc))    % process is to long?  
                
                fprintf("\n *** Phase 2 to long. Process %i core %i will be repeated. \n", ProcesNum, ProcesCore);
         		
                kill_subs(ControlMatrix(ProcesNum,9));
                fclose(ControlMatrix(ProcesNum,7));
                fclose(ControlMatrix(ProcesNum,8));
                waitpid(ControlMatrix(ProcesNum,9));    % zamknij
                
                CoresMatrix(ProcesCore,1)=0;     % reset
                CoresMatrix(ProcesCore,3)=0;
                ControlMatrix(ProcesNum,1)=0;    % reset
                ControlMatrix(ProcesNum,2)=0;    % reset
                ControlMatrix(ProcesNum,3)=1;    % repeated
                ControlMatrix(ProcesNum,4)=ControlMatrix(ProcesNum,4)+1;
                                      % increase correction
                if (ControlMatrix(ProcesNum,4)>4) 
                    ControlMatrix(ProcesNum,1)=-1;    % malfunction
                    ControlMatrix(ProcesNum,2)=3;    % ready
                end
                
                                      
                                      
		pause(0.5);
		[err,msg]=unlink(Ph2FlagF);
   
            end
                         
        end
        
    end
    
end

% point 4 - check if work finished and present results

N1=size((find(ControlMatrix(:,1)==(-1))),1);
N2=size((find(ControlMatrix(:,1)==2)),1);
N3=size(ControlMatrix,1);

if N1+N2>=N3
   WorkNotFinished=0;       % main while loop is done
end

pause(0.5);

if (time()-Time_pt)>10      % each 10 seconds
    Time_pt=time();

    clc;
    fprintf('\n\n### FTECTmain ###\n\n');
    fprintf('Process time: %4.2f (min)\n',(time()-Time_st)./60);
    fprintf('Process time: %4.2f (hours)\n\n',(time()-Time_st)./3600);

    fprintf('Total: %i Done: %i Failed: %i\n\n',N3,N2,N1);
    for i=1:NoCores,

        %ProcTV=ControlMatrix(CoresMatrix(i,2),6)-ControlMatrix(CoresMatrix(i,2),5);
        if (CoresMatrix(i,1)==1)
            ProcTV=time()-ControlMatrix(CoresMatrix(i,2),5);
            else
            ProcTV=0;
        end
       
        % fprintf('Core: %2i Process: %6i State: %1i Phase: %1i Resol: %1i Time: %4.0f (s)\n',i,CoresMatrix(i,2),CoresMatrix(i,1),CoresMatrix(i,3),ControlMatrix(CoresMatrix(i,2),4),ProcTV);
    end
    
    fprintf('\n\nInput matrix size %i %i \n',size(InputMatrix,1), size(InputMatrix,2));
    
    FID_TV = fopen('AAA_state.txt', 'w');
    fprintf(FID_TV,'FTECTmain\n\n');
    fprintf(FID_TV,'Process time: %4.2f (min)\n',(time()-Time_st)./60);
    fprintf(FID_TV,'Process time: %4.2f (hours)\n\n',(time()-Time_st)./3600);

    fprintf(FID_TV,'Total: %i Done: %i Failed: %i\n\n',N3,N2,N1);
    for i=1:NoCores,

        %ProcTV=ControlMatrix(CoresMatrix(i,2),6)-ControlMatrix(CoresMatrix(i,2),5);
        if (CoresMatrix(i,1)==1)
            ProcTV=time()-ControlMatrix(CoresMatrix(i,2),5);
            else
            ProcTV=0;
        end
       
        fprintf(FID_TV,'Core: %2i Process: %6i State: %1i Phase: %1i Resol: %1i Time: %4.0f (s)\n',i,CoresMatrix(i,2),CoresMatrix(i,1),CoresMatrix(i,3),ControlMatrix(CoresMatrix(i,2),4),ProcTV);
    end
    fprintf(FID_TV,'\n\nInput Matrix size %i %i \n',size(InputMatrix,1), size(InputMatrix,2));
    fclose(FID_TV);
    
end

end	% end of main while


save _ControlMatrix.txt ControlMatrix
save _InputMatrix.txt InputMatrix
save _OutputMatrix.txt OutputMatrix
save _Steps.txt y_min y_step y_max fi_min fi_step fi_max

save -binary _ControlMatrix.mat ControlMatrix
save -binary _InputMatrix.mat InputMatrix
save -binary _OutputMatrix.mat OutputMatrix
save -binary _Steps.mat y_min y_step y_max fi_min fi_step fi_max
fprintf('\n\n All work done. \n\n');


diary off;


