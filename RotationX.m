function [ o1, o2, o3 ] = RotationX( i1, i2, i3, fi )
%ROTATIONX Summary of this function goes here
%   Detailed explanation goes here

Rx=[1 0 0; 0 cos(fi) -1.*sin(fi); 0 sin(fi) cos(fi)];

out=Rx*([i1 i2 i3]');

o1=out(1);
o2=out(2);
o3=out(3);

end

