function [ in, out, pid ] = StartPhase2(core)
%STRARTPROCESS Move the process - phase 2

% fprintf('\n Process to Phase 2(core): %1.0f \n',core);

% start Elmer batch

D1=['Work',num2str(core,'%03i')];

cd(D1);

[in, out, pid]=popen2("./phase2_go");

cd ('..');

end

