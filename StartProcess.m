function [ PSt, in, out, pid ] = StartProcess( y, fi, core )
%STRARTPROCESS Start the process - phase 1

% fprintf('\n StartProcess (y,fi,core): %2.3f , %2.3f , %1.0f \n',y,fi,core);

% Clear space of core

D1=['Work',num2str(core,'%03i')];

cd(D1);

[output, text]=system("./clearspace"); 

cd ('..');

% Crate .geo file

D1=['Work',num2str(core,'%03i'),'/perm_p2'];

GEOWriteYfiX(D1,y,fi./360.*2.*pi);

% start Netgen batch

D1=['Work',num2str(core,'%03i')];

cd(D1);

[in, out, pid]=popen2("./phase1_go");

cd ('..');

PSt=time();

end

