MODULE interpUtils

    Use DefUtils
    Implicit None
	REAL(KIND=dp) :: &delta_step = 0.01d0 ,& 	
	arr_db = 1d0 ,&
	arr_ub = 16d0 ,&
	x_db = -0.015d0 ,&
	x_ub = 0.015d0 ,&
	y_db = -0.045d0 ,&
	y_ub = 0.045d0
	
    contains
	
!***********************************************************
!
SUBROUTINE change_range(x, y, xmin, xmax, ymin_o, ymax_o, delta, &
						arr_min, arr_max, x2, y2, interpolate)
!
!Shift and scale ELMER Body coordinates for interpolation array
!
!***********************************************************

!recalculates to fit the range of array
!x/y - current ELMER Body coordinates
!xmin/max - range of ELMER Body in one direction
!ymin_o/max_o - range of ELMER Body in movement direction
!delta - liner shift step (array shift)
!arr_min/max - size of interpolating array

!xmin = -0.05; xmax = 0.05; ymin_o = -0.045, ymax = 0.045; delta = 0;
!arr_min = 0; arr_max = 16;
! ***ymin_o to distinguish from moved ymin***

	Implicit None
	REAL(KIND=dp), intent(in) :: x, y, xmin, xmax, ymin_o, ymax_o, delta, &
							arr_min, arr_max
	REAL(KIND=dp) :: ymin, ymax
	REAL(KIND=dp), intent(out) :: x2, y2
	LOGICAL, intent(out) :: interpolate

	ymin = ymin_o + delta
	ymax = (xmax - xmin) + ymin
	
	!Check if y is out of array bounds, or array is out of body bounds
	if ((y > ymax) .OR. (ymax > ymax_o) .OR. (y < ymin) &
				.OR. (x > xmax) .OR. (x < xmin)) then
		interpolate = .FALSE.
	else
		interpolate = .TRUE.
	end if 
	
	x2 = arr_min + (x - xmin) * (arr_max - arr_min) / (xmax - xmin)
	y2 = arr_min + (y - ymin) * (arr_max - arr_min) / (ymax - ymin)

END SUBROUTINE change_range

!***********************************************************
!
SUBROUTINE spindle_gen(arrsize, r, posval, negval, arr)
!
!***********************************************************

Implicit None
REAL(KIND=dp), intent(inout) :: arr(:, :)
REAL(KIND=dp), intent(in):: arrsize, r, posval, negval
REAL(KIND=dp) :: l
INTEGER ::  mid_x, mid_y, i, j


mid_x = ceiling(arrsize / 2)
mid_y = mid_x

do i = 1, floor(arrsize)
	do j = 1, floor(arrsize)
	
		l = sqrt(REAL((i - mid_x))**2 + REAL((j - mid_y))**2)
		If(l <= r) then
			arr(i,j) = posval
		else
			arr(i,j) = negval
		end if
	end do
end do

END SUBROUTINE spindle_gen

!***********************************************************
!
!Here goes interpolation function
!
!***********************************************************

!***********************************************************
!
!Dummy interpolation function
!
!***********************************************************

FUNCTION interp2(x, y, arr) RESULT(intval)

	Use defutils
	Implicit None
	REAL(KIND=dp), intent(in ):: x, y, arr(:, :)
	REAL(KIND=dp) :: intval
	
	intval = 1d0

END FUNCTION interp2
!***************************************************
FUNCTION interp(x, y, arr) RESULT(interpval)
!interpolates in (x,y) over given array (arr)

	Use DefUtils
	Implicit None
	INTEGER :: x_up, x_down, y_up, y_down
	REAL(KIND=dp) :: interpval, interpval1, interpval2, norm, &
					q11, q12, q21, q22, &
					coord11, coord12, coord21, coord22
	REAL(KIND=dp), intent(in) :: x, y, arr(:, :)
						
	!Searching for coordinates of known points 
	!between with interpolated point lays
	x_up = ceiling(x)
	x_down = floor(x)
	y_up = ceiling(y)
	y_down = floor(y)
		
	!Getting values of array for points 
	!between with interpolated point lays
	q11 = arr(x_down, y_down)
	q12 = arr(x_down, y_up)
	q21 = arr(x_up, y_down)
	q22 = arr(x_up, y_up)
	
	!Calculating differences of interpolated point 
	!and known points coordinates
	coord11 = (Real(x_up) - x) * (Real(y_up) - y)
	coord12 = (Real(x_up - x)) * (y - Real(y_down))
	coord21 = (x - Real(x_down)) * (Real(y_up) - y)
	coord22 = (x - Real(x_down)) * (y - Real(y_down))
	
	!Calculating normalisation parameter
	norm = 1d0 !(Real(x_up - x_down)) * (Real(y_up - y_down))
	
	!Calculating value of interpolated point
	!interpval2 = (q11 * coord11 + q12 * coord12 + q21 * coord21 & 
	!				+ q22 * coord22)/norm
	interpval1 = arr(floor(x), floor(y))
	interpval2 = arr(ceiling(x), ceiling(y))
	if(coord11 > coord22) then
		interpval = interpval1
	else
		interpval = interpval2
	end if
								
END FUNCTION interp

END MODULE interpUtils

!***********************************************************
!
FUNCTION rel_permeability(model, n, args) RESULT(rel_perm)
!
!function calculates interpolated relative permeability from array of values
!
!***********************************************************

	Use interpUtils
	Implicit None
	Type(model_t) :: model
	Integer :: n
	REAL(KIND=dp) :: x, y, z, t, args(4), rel_perm, x2, y2
	LOGICAL :: interpolate
	REAL(kind=dp), DIMENSION(:, :), Allocatable :: arr
	integer :: AllocateStatus, DeAllocateStatus
	integer, parameter :: max_plane_size = 16
	
	!Allocate 16x16 array
	allocate(arr(max_plane_size,max_plane_size),STAT=AllocateStatus)
       IF (AllocateStatus /= 0) STOP "Not enough memory for Array"
	   
		CALL spindle_gen(16d0, 7d0, 1000d0, 1d0, arr)
		
	x = args(1)
	y = args(2)
	z = args(3)
	t = args(4)
	
	!convert from body range to array range (see subroutine definition)
	!CALL change_range(x, y, xmin, xmax, ymin_o, ymax, delta, &
	!					arr_min, arr_max, x2, y2, interpolate)
	CALL change_range(z, y, x_db, x_ub, y_db, y_ub, delta_step, &
						arr_db, arr_ub, x2, y2, interpolate)
		
	!interpolate value from x2, y2 over array (arr)
	!If not out of bounds then interpolate, else do not
	IF (interpolate) THEN
		rel_perm = interp(x2, y2, arr)
	ELSE
		rel_perm = 1
	END IF
	
	DEALLOCATE (arr, STAT = DeAllocateStatus)
	
END FUNCTION rel_permeability

!***********************************************************
!
FUNCTION el_conductivity(model, n, args) RESULT(el_cond)
!
!function calculates interpolated electric conductivity from array of values
!
!***********************************************************

	Use interpUtils
	Implicit None
	Type(model_t) :: model
	Integer ::n
	REAL(KIND=dp) :: x, y, z, t, args(4), el_cond, x2, y2
	LOGICAL :: interpolate
	REAL(KIND=dp), DIMENSION(:, :), Allocatable :: arr(:, :)
	integer :: AllocateStatus, DeAllocateStatus
	integer, parameter :: max_plane_size = 16
	
	!Allocate 16x16 array
	allocate(arr(max_plane_size,max_plane_size),STAT=AllocateStatus)
       IF (AllocateStatus /= 0) STOP "Not enough memory for Array"
	   
		CALL spindle_gen(16d0, 7d0, 1.45d6, 0d0, arr)
		
	x = args(1)
	y = args(2)
	z = args(3)
	t = args(4)
	
	!convert from body range to array range
	CALL change_range(z, y, x_db, x_ub, y_db, y_ub, delta_step, &
						arr_db, arr_ub, x2, y2, interpolate)
	!Print*, z,y, x2,y2, interpolate
	!CALL change_range(x, y, xmin, xmax, ymin_o, ymax, delta, &
	!					arr_min, arr_max, x2, y2, interpolate)
		
	!interpolate value from x2, y2 over array (arr)
	!If not out of bounds then interpolate, else do not
	IF (interpolate) THEN
		el_cond = interp(x2, y2, arr)
	ELSE
		el_cond = 0
	END IF
	
	DEALLOCATE (arr, STAT = DeAllocateStatus)
END FUNCTION el_conductivity


