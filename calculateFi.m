function [ Fi_re, Fi_im ] = calculateFi( file_name )

main_matrix=load(file_name);

main_matrix(:,[1:3 7 8 10 11])=[];

grid_x=-0.02:(0.04/42):0.02;
grid_y=-0.02:(0.04/42):0.02;
grid_z=0.018:0.002:0.024;

sorted_matrix_re=zeros(size(grid_x,2), size(grid_y,2), size(grid_z,2));
sorted_matrix_im=zeros(size(grid_x,2), size(grid_y,2), size(grid_z,2));

grid_sorted_matrix_x=zeros(size(grid_x,2), size(grid_y,2), size(grid_z,2));
grid_sorted_matrix_y=zeros(size(grid_x,2), size(grid_y,2), size(grid_z,2));
grid_sorted_matrix_y=zeros(size(grid_x,2), size(grid_y,2), size(grid_z,2));


grid_tz=grid_z;
grid_tr=0.006:0.002:0.016;

integrated_matrix_re=zeros(size(grid_tz,2), size(grid_tr,2));
integrated_matrix_im=zeros(size(grid_tz,2), size(grid_tr,2));


for i=1:size(main_matrix,1),

    vect=main_matrix(i,:);

    dist_x=(grid_x-vect(1)).^2;
    dist_y=(grid_y-vect(2)).^2;
    dist_z=(grid_z-vect(3)).^2;

    [a,pos_x]=min(dist_x);
    [a,pos_y]=min(dist_y);
    [a,pos_z]=min(dist_z);

    sorted_matrix_re(pos_x, pos_y, pos_z)=vect(4);
    sorted_matrix_im(pos_x, pos_y, pos_z)=vect(5);

    grid_sorted_matrix_x(pos_x, pos_y, pos_z)=vect(1);
    grid_sorted_matrix_y(pos_x, pos_y, pos_z)=vect(2);
    grid_sorted_matrix_z(pos_x, pos_y, pos_z)=vect(3);
    
    for j=1:size(grid_tr,2),
    
        if (sqrt((vect(1).^2+vect(2).^2))<grid_tr(j))
            
            integrated_matrix_re(pos_z,j)= integrated_matrix_re(pos_z,j)+vect(4);
            integrated_matrix_im(pos_z,j)= integrated_matrix_im(pos_z,j)+vect(5);
                     
        end
    end
        
end

dx=diff(squeeze(grid_sorted_matrix_x(:,5,1)));
dy=diff(squeeze(grid_sorted_matrix_y(5,:,1))');
grid_ds=mean(dx).*mean(dy);

integrated_matrix_re=integrated_matrix_re.*grid_ds;
integrated_matrix_im=integrated_matrix_im.*grid_ds;

grid_cz=0.017:(0.02486-0.017)/18:0.02486;
grid_cr=0.01485/2:(0.0296/2-0.01485/2)/18:0.0296/2;

[XI,YI] = meshgrid(grid_cr, grid_cz);
[X,Y] = meshgrid(grid_tr, grid_tz);

integrated_matrixI_re=interp2(X,Y,integrated_matrix_re,XI,YI,'cubic');
integrated_matrixI_im=interp2(X,Y,integrated_matrix_im,XI,YI,'cubic');

integrated_matrixI_re=inpaint_nans(integrated_matrixI_re,3);
integrated_matrixI_im=inpaint_nans(integrated_matrixI_im,3);

Fi_re=sum(sum(integrated_matrixI_re));
Fi_im=sum(sum(integrated_matrixI_im));


end

