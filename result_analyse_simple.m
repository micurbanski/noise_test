TableB=load("_OutputMatrix.mat","-binary");
TableC=load("_ControlMatrix.mat","-binary");

n_org=size(TableB,1);


y1=TableB(:,1)';
y2=TableB(:,2)';


% extension begining and finishing

y1e=[y1 y1 y1 y1 y1];
y2e=[y2 y2 y2 y2 y2];


% filtering

%f_sampling=1./n_org;

%f_filter=1./25.*f_sampling;

%[b,a] = butter(5,2*f_filter/f_sampling);

%y1f=filtfilt(b,a,y1e);
%y2f=filtfilt(b,a,y2e);

y1ff=0.*y1e;
y2ff=0.*y1e;

y1fb=0.*y1e;
y2fb=0.*y1e;

alpha=0.21;

for i=2:size(y1e,2)
  y1ff(i)=alpha.*y1e(i)+(1-alpha).*y1ff(i-1);
  y2ff(i)=alpha.*y2e(i)+(1-alpha).*y2ff(i-1);
  
end;

for i=size(y1e,2)-1:-1:1
  y1fb(i)=alpha.*y1e(i)+(1-alpha).*y1fb(i+1);
  y2fb(i)=alpha.*y2e(i)+(1-alpha).*y2fb(i+1);
  
end;

y1f=(y1fb+y1ff)./2;
y2f=(y2fb+y2ff)./2;

% select like original

y1r=y1f(2.*n_org+1:3.*n_org);
y2r=y2f(2.*n_org+1:3.*n_org);


% plot

t=1:n_org;

plot(t,[y1;y1r;y2;y2r]);

% estimate signal quality 

max_re=max(y1r)./n_org;
max_im=max(y2r)./n_org;

min_re=min(y1r)./n_org;
min_im=min(y2r)./n_org;

mean_re=mean(y1r)./n_org;
mean_im=mean(y2r)./n_org;

quality_re=sum((y1-y1r).^2)./n_org;
quality_im=sum((y2-y2r).^2)./n_org;

fprintf('\n\nQuality analyse:\n');
fprintf('Max     im: %e, re:%e \n',max_re, max_im);
fprintf('Min     im: %e, re:%e \n',min_re, min_im);
fprintf('Mean    im: %e, re:%e \n',mean_re, mean_im);
fprintf('Quality im: %e, re:%e \n\n',quality_re, quality_im);

correction_mean=mean(TableC(:,4));
time_mean=mean(TableC(:,6)-TableC(:,5));

% hist(TableC(:,6)-TableC(:,5),20);

fprintf('Efficiency analyse:\n');
fprintf('Avg. correction request: %1.3f \n',correction_mean);
fprintf('Avg. calculation time:   %4.1f s.\n\n',time_mean);


fid=fopen('_Analyse.txt','w');

fprintf(fid,'\n\nQuality analyse:\n');
fprintf(fid,'Max     im: %e, re:%e \n',max_re, max_im);
fprintf(fid,'Min     im: %e, re:%e \n',min_re, min_im);
fprintf(fid,'Mean    im: %e, re:%e \n',mean_re, mean_im);
fprintf(fid,'Quality im: %e, re:%e \n\n',quality_re, quality_im);
fprintf(fid,'Efficiency analyse:\n');
fprintf(fid,'Avg. correction request: %1.3f \n',correction_mean);
fprintf(fid,'Avg. calculation time:   %4.1f s.\n\n',time_mean);

fclose(fid); % close
