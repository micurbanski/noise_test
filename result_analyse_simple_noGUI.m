load _OutputMatrix.mat;
load _ControlMatrix.mat;
TableB=OutputMatrix;
TableC=ControlMatrix;

n_org=size(TableB,1);


y1=TableB(:,1)';
y2=TableB(:,2)';


% extension begining and finishing

y1e=y1;
y2e=y2;

y1ff=y1e;
y2ff=y2e;

y1fb=y1e;
y2fb=y2e;

alpha=0.25;

for i=2:size(y1e,2)
  y1ff(i)=alpha.*y1e(i)+(1-alpha).*y1ff(i-1);
  y2ff(i)=alpha.*y2e(i)+(1-alpha).*y2ff(i-1);
  
end;

for i=size(y1e,2)-1:-1:1
  y1fb(i)=alpha.*y1e(i)+(1-alpha).*y1fb(i+1);
  y2fb(i)=alpha.*y2e(i)+(1-alpha).*y2fb(i+1);
  
end;

y1f=(y1fb+y1ff)./2;
y2f=(y2fb+y2ff)./2;

% select like original

y1r=y1f;
y2r=y2f;


% plot

t=1:n_org;

% plot(t,[y1;y1r;y2;y2r]);

% estimate signal quality 

max_re=max(y1r);
max_im=max(y2r);

min_re=min(y1r);
min_im=min(y2r);

mean_re=mean(y1r);
mean_im=mean(y2r);

quality_re=sum((y1-y1r).^2)./n_org;
quality_im=sum((y2-y2r).^2)./n_org;

noise_re=sum(abs(y1-y1r)./mean_re)./n_org.*100;
noise_im=sum(abs(y2-y2r)./mean_im)./n_org.*100;

noise_maxre=max(abs(y1-y1r)./mean_re).*100;
noise_maxim=max(abs(y2-y2r)./mean_im).*100;

fprintf('\n\nQuality analyse:\n');
fprintf('Max       im: %e, re:%e \n',max_re, max_im);
fprintf('Min       im: %e, re:%e \n',min_re, min_im);
fprintf('Mean      im: %e, re:%e \n',mean_re, mean_im);
fprintf('Noise avg im: %1.2f%%, re:%1.2f%%\n',noise_re, noise_im);
fprintf('Noise max im: %1.2f%%, re:%1.2f%%\n',noise_maxre, noise_maxim);
fprintf('Quality   im: %e, re:%e \n\n',quality_re, quality_im);

correction_mean=mean(TableC(:,4));
time_mean=mean(TableC(:,6)-TableC(:,5));

% hist(TableC(:,6)-TableC(:,5),20);

fprintf('Efficiency analyse:\n');
fprintf('Avg. correction request: %1.3f \n',correction_mean);
fprintf('Avg. calculation time:   %4.1f s.\n\n',time_mean);


fid=fopen('_Analyse.txt','w');

fprintf(fid,'\n\nQuality analyse:\n');
fprintf(fid,'Max       im: %e, re:%e \n',max_re, max_im);
fprintf(fid,'Min       im: %e, re:%e \n',min_re, min_im);
fprintf(fid,'Mean      im: %e, re:%e \n',mean_re, mean_im);
fprintf(fid,'Noise avg im: %1.2f%%, re:%1.2f%%\n',noise_re, noise_im);
fprintf(fid,'Noise max im: %1.2f%%, re:%1.2f%%\n',noise_maxre, noise_maxim);
fprintf(fid,'Quality   im: %e, re:%e \n\n',quality_re, quality_im);
fprintf(fid,'Efficiency analyse:\n');
fprintf(fid,'Avg. correction request: %1.3f \n',correction_mean);
fprintf(fid,'Avg. calculation time:   %4.1f s.\n\n',time_mean);

fclose(fid); % close
